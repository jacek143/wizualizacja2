#-------------------------------------------------
#
# Project created by QtCreator 2015-12-06T12:12:41
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = wizualizacja2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    crc.c

HEADERS  += mainwindow.h \
    crc.h

FORMS    += mainwindow.ui
