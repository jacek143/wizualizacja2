#include "crc.h"

uint8_t crc8(uint8_t data[], uint8_t len) {
    const uint8_t *ptr = data;
    unsigned crc = 0;
    uint8_t i, j;
    for (j = len; j; j--, ptr++) {
        crc ^= (*ptr << 8);
        for(i = 8; i; i--) {
            if (crc & 0x8000)
                crc ^= (0x1070 << 3);
            crc <<= 1;
        }
    }
    return (uint8_t)(crc >> 8);
}
