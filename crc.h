#ifndef CRC_H
#define CRC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/**
 * CRC8 using x^8 + x^2 + x + 1 polynomial
 */
uint8_t crc8(uint8_t data[], uint8_t len);

#ifdef __cplusplus
}
#endif

#endif // CRC_H

