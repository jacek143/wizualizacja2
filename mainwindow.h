#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort>
#include <stdint.h>
#include "crc.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void resetSerial(void);
    void startStopRobot(void);
    void collectData(void);

private:
    void displayData(QByteArray frame);
    void initSerial(void);
    QSerialPort _serialPort;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
