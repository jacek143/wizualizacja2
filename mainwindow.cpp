#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->resetButton,SIGNAL(released()),this,SLOT(resetSerial()));
    connect(ui->startStopButton, SIGNAL(released()), this, SLOT(startStopRobot()));
    connect(&_serialPort, SIGNAL(readyRead()), this, SLOT(collectData()));

    initSerial();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief MainWindow::resetSerial Reset serial port.
 */
void MainWindow::resetSerial(void){
    _serialPort.close();
    _serialPort.open(QIODevice::ReadWrite);
}

/**
 * @brief MainWindow::startStopRobot Start/stop robot.
 */
void MainWindow::startStopRobot(void){
    _serialPort.write(" ");
}

/**
 * @brief MainWindow::initSerial - Initialize and open serial port.
 */
void MainWindow::initSerial(void){
    _serialPort.setBaudRate(QSerialPort::Baud9600);
    _serialPort.setDataBits(QSerialPort::Data8);
    _serialPort.setFlowControl(QSerialPort::NoFlowControl);
    _serialPort.setPortName("COM6");
    _serialPort.setParity(QSerialPort::NoParity);
    _serialPort.setStopBits(QSerialPort::OneStop);
    _serialPort.open(QIODevice::ReadWrite);
}

/**
 * @brief displayData Display data from the frame
 * @param frame - Frame with data to display.
 */
void MainWindow::displayData(QByteArray frame){
    uint16_t values[12];
    for(unsigned i = 0; i < 12; i++){
        values[i] = 256 * ((uint8_t) frame[2*i+2]) + ((uint8_t) frame[2*i+1]);
    } // for

    ui->lcdIrL->display(values[0]);
    ui->progressBarIrL->setValue(values[0]);

    ui->lcdIrLf->display(values[1]);
    ui->progressBarIrLf->setValue(values[1]);

    ui->lcdIrRf->display(values[2]);
    ui->progressBarIrRf->setValue(values[2]);

    ui->lcdIrR->display(values[3]);
    ui->progressBarIrR->setValue(values[3]);


    ui->lcdObjL->display(values[4]);
    ui->progressBarObjL->setValue(values[4]);

    ui->lcdObjLf->display(values[5]);
    ui->progressBarObjLf->setValue(values[5]);

    ui->lcdObjRf->display(values[6]);
    ui->progressBarObjRf->setValue(values[6]);

    ui->lcdObjR->display(values[7]);
    ui->progressBarObjR->setValue(values[7]);


    ui->lcdAmbL->display(values[8]);
    ui->progressBarAmbL->setValue(values[8]);

    ui->lcdAmbLf->display(values[9]);
    ui->progressBarAmbLf->setValue(values[9]);

    ui->lcdAmbRf->display(values[10]);
    ui->progressBarAmbRf->setValue(values[10]);

    ui->lcdAmbR->display(values[11]);
    ui->progressBarAmbR->setValue(values[11]);
}

/**
 * @brief MainWindow::collectData Collect and display data.
 *
 * This function reads data from the serial port.
 * When it finishes to collect frame, it invokes MainWindow#displayData.
 */
void MainWindow::collectData(void){
    const unsigned frameLength = 26;
    static QByteArray frame = QByteArray(frameLength,0);
    static unsigned i = 0;

    QByteArray allData = _serialPort.readAll();

    while(allData.size()){
        if(i == 0){
            if(allData.at(0) == '!'){
                frame[0] = '!';
                i++;
            }
        }
        else{
            frame[i++] = allData.at(0);
        }
        allData.remove(0,1);
        if(i == frameLength){
            uint8_t uframe[frameLength-1];
            for(unsigned k = 0; k < frameLength-1; k++){
                uframe[k] = frame.at(k);
            }
            if(frame[frameLength-1] == (char) crc8(uframe, frameLength-1)){
                displayData(frame);
            }
            _serialPort.clear();
            allData.clear();
            i = 0;
        }
    }
}
